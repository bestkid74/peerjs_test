window.addEventListener('load', () => {
  var localStream;
  var remoteStream;
  var chatEnded = document.querySelector('#chatEnded');
  var localVideo = document.querySelector('#localVideo');
  var remoteVideo = document.querySelector('#remoteVideo');
  var call;

  var peer = new Peer('elegro-2', {
    key: 'elegro',
    config: {
      'iceServers': [
        { url:'stun:18.221.165.117:3478' },
        {
          url: 'turn:18.221.165.117:3478',
          credential: 'test',
          username: '123456',
        },
      ],
    },
    debug: 3
  });

  navigator.mediaDevices.getUserMedia({
    audio: true,
    video: {mandatory: {maxWidth: 320, maxHeight: 240}}
  })
  .then(stream => {
    localStream = stream;
    localVideo.srcObject = stream;
    localVideo.play();
    call = peer.call('elegro-1', localStream);
    call.on('stream', function(remstream) {
      console.log('Get remote stream');
      remoteVideo.srcObject = remstream;
      remoteStream = remstream;
    });
  })
  .catch(e => {
    console.log(e);
  });

  peer.on('open', function(id) {
    console.log('My peer ID is: ' + id);
  });

  peer.on('error', function (err) {
    console.log('ERR= ', err);
  });

  var leaveChat = document.getElementById('leave-chat-btn');
  leaveChat.onclick = function() {
    chatEnded.style = 'opacity: 1';
    call.close();
  };
});
